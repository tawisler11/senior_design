Rails.application.routes.draw do

  get 'battery_charges/new'

  get 'battery_charges/create'

  root 'static_pages#home'
  get 'help' => 'static_pages#help'
  
  resources :users
  resources :load_states, only: [:new, :show, :create]
  resources :battery_charges, only: [:new, :create]

  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  
  get 'newLoad'     => 'users#newLoad'
  post 'createLoad' => 'users#createLoad'
  get 'profile'     => 'users#profile'
  
 
end