class BatteryCharge < ActiveRecord::Base
    belongs_to :user
    
    default_scope -> { order('created_at DESC') }
    validates :charge, presence: true, length: { maximum: 100 }
end
