class BatteryChargesController < ApplicationController
  
  def new
        if logged_in?
            @battery_charge = current_user.battery_charges.build
        else
            flash[:danger] = "Please log in."
            redirect_to root_url
        end
  end
    
  def create
      @battery_charge = current_user.battery_charges.build(battery_charge_params)
      if @battery_charge.save
          flash[:success] = "Battery Charge created!"
          redirect_to current_user
      else
          render 'battery_charges/new'
      end
  end
    
  def show
  end
    
    private
    
        def battery_charge_params
            params.require(:battery_charge).permit(:charge)
        end
        
end
