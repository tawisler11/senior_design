class LoadStatesController < ApplicationController
    
    
    def new
        if logged_in?
            @load_state = current_user.load_states.build
        else
            flash[:danger] = "Please log in."
            redirect_to root_url
        end
    end
    
    def create
        @load_state = current_user.load_states.build(load_state_params)
        if @load_state.save
            flash[:success] = "Load State created!"
            redirect_to current_user
        else
            render 'load_states/new'
        end
    end
    
    def show
    end
    
    private
    
        def load_state_params
            params.require(:load_state).permit(:load, :action)
        end
    
end
