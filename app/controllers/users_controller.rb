class UsersController < ApplicationController
  
  before_action :logged_in_user, only: [:edit, :update, :show]
  before_action :correct_user,   only: [:edit, :update, :show]
  
  def new
  end
  
  def create
    @user = User.new(user_params)    # Not the final implementation!
    if @user.save
      flash[:info] = "Thank you! Please log in."
      redirect_to root_url
    else
      flash[:danger] = "An error occurred."
      render 'static_pages/home'
    end
  end
  
  def show
    load1 = " SELECT * 
        FROM   load_states
        WHERE  load = '1'
        ORDER BY created_at DESC "
    load2 = " SELECT * 
        FROM   load_states
        WHERE  load = '2'
        ORDER BY created_at DESC "
    load3 = " SELECT * 
        FROM   load_states
        WHERE  load = '3'
        ORDER BY created_at DESC "
    load4 = " SELECT * 
        FROM   load_states
        WHERE  load = '4'
        ORDER BY created_at DESC "
        
    @user = User.find(params[:id])
    
    @load_states_1 = @user.load_states.paginate_by_sql(load1, page: params[:page], :per_page => 3)
    @load_states_2 = @user.load_states.paginate_by_sql(load2, page: params[:page], :per_page => 3)
    @load_states_3 = @user.load_states.paginate_by_sql(load3, page: params[:page], :per_page => 3)
    @load_states_4 = @user.load_states.paginate_by_sql(load4, page: params[:page], :per_page => 3)
    
    @battery_charges = @user.battery_charges.paginate(page: params[:page], :per_page => 10)
  end
  
  def profile
    if logged_in?
      redirect_to current_user
    else
      flash[:danger] = "Please log in."
      redirect_to root_url
    end
  end
  
  def newLoad
    @user = User.new
    @load_state = @user.load_states.build
  end
  
  def createLoad
    @user = User.find_by(email: params[:user][:email].downcase)
    @load_state = @user.load_states.build(load_state_params)
    
    if @user && @user.authenticate(params[:user][:password])
      #Create load
      if @user.update(load_state_params)
        flash[:success] = "Load State created!"
        redirect_to root_url
      else
        flash[:danger] = 'Load not created.'
        redirect_to root_url
      end
      
    else
      flash[:danger] = 'Invalid email/password combination'
      redirect_to root_url
    end
  end
  
  private
    
    def load_state_params
      params.require(:user).permit(:load_state)
    end
    
    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end
    
     # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to root_url
      end
    end

    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(current_user) unless current_user?(@user)
    end
  
end