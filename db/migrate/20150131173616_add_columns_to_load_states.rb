class AddColumnsToLoadStates < ActiveRecord::Migration
  def change
    add_column :load_states, :load, :integer
    add_column :load_states, :date, :date
    add_column :load_states, :time, :time
    add_column :load_states, :action, :boolean
  end
end
