class AddUserIdToLoadState < ActiveRecord::Migration
  def change
    add_column :load_states, :user_id, :integer
  end
end
