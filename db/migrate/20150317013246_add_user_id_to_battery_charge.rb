class AddUserIdToBatteryCharge < ActiveRecord::Migration
  def change
    add_column :battery_charges, :user_id, :integer
  end
end
