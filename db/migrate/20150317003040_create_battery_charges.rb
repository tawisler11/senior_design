class CreateBatteryCharges < ActiveRecord::Migration
  def change
    create_table :battery_charges do |t|
      t.integer :charge
      t.string :date

      t.timestamps null: false
    end
  end
end
