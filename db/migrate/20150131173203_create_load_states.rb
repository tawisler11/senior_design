class CreateLoadStates < ActiveRecord::Migration
  def change
    create_table :load_states do |t|

      t.timestamps null: false
    end
  end
end
